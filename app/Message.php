<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Crypt;

/**
 * @property string message
 */
class Message extends Model
{

    protected $guarded = [];

    public static function encryptWithKey($from, $message, $key)
    {
        $key = self::generateKey($key);

        return self::create([
            'from' => $from,
            'message' => (new Encrypter($key))->encryptString($message),
        ]);
    }

    public static function generateKey($key)
    {
        return str_pad(str_limit(md5($key), 16, ''), 16, ' ');
    }
}
