<?php

namespace App\Http\Controllers;

use App\Http\DecryptedMessage;
use App\Http\Requests\DecryptMessageRequest;
use App\Http\Responses\DecryptionFailedResponse;
use App\Http\Responses\MessageDecryptedResponse;
use App\Http\Responses\MessageEncryptedResponse;
use App\Message;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Encryption\EncryptException;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    public function showAll()
    {
        $messages = Message::query()->orderByDesc('id')->get();

        return view('message.list')->with('messages', $messages);
    }

    public function encrypt(Request $request)
    {
        $message = Message::encryptWithKey(
            $request->get('from'),
            $request->get('message'),
            $request->get('emoji-key')
        );

        return new MessageEncryptedResponse($message);
    }

    public function decrypt(DecryptMessageRequest $request, $id)
    {
        /** @var Message $message */
        $message = Message::query()->findOrFail($id);

        try {
            $decrypted = new DecryptedMessage($message, $request->get('emoji-key'));

            return new MessageDecryptedResponse($decrypted);
        } catch (DecryptException $exception) {
            return new DecryptionFailedResponse();
        }
    }

}
