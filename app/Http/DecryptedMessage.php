<?php

namespace App\Http;

use App\Message;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Encryption\Encrypter;

class DecryptedMessage implements Jsonable, Arrayable
{

    /**
     * @var Message
     */
    public $message;
    public $plaintext;

    public function __construct(Message $message, $key)
    {
        $this->message = $message;
        $key = Message::generateKey($key);

        $this->plaintext = (new Encrypter($key))->decryptString($message->message);
    }

    public function __toString()
    {
        return $this->plaintext;
    }

    public function toArray()
    {
        return [
            'from' => $this->message->from,
            'ciphertext' => $this->message->message,
            'plaintext' => $this->plaintext
        ];
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }
}
