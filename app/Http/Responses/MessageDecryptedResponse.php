<?php

namespace App\Http\Responses;

use App\Http\DecryptedMessage;
use Illuminate\Contracts\Support\Responsable;

class MessageDecryptedResponse implements Responsable
{

    /**
     * @var DecryptedMessage
     */
    private $decryptedMessage;

    public function __construct(DecryptedMessage $decryptedMessage)
    {
        $this->decryptedMessage = $decryptedMessage;
    }

    public function toResponse($request)
    {
        if ($request->ajax()) {
            return response()->json([ 'success' => true, 'message' => $this->decryptedMessage ]);
        }

        return view('message.show')->with('decrypted_message', $this->decryptedMessage);
    }
}
