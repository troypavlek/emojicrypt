<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MessageController@showAll')->name('messages.list');
Route::post('/message/{id}/decrypt', 'MessageController@decrypt')->name('messages.decrypt');
Route::post('/message', 'MessageController@encrypt')->name('messages.encrypt');
