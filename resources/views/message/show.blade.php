@extends('layout')

@section('title', "View Message")

@section('content')
    <h1>Message from {{ $decrypted_message->message->from }}</h1>

    <div>
        {{ $decrypted_message->plaintext }}
    </div>
@stop
