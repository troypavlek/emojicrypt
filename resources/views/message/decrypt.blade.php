<form action="{{ URL::route('messages.decrypt', $message_id) }}" method="post">
    @csrf

    <div>
        <label for="emoji-key">Emoji Key</label>
        <input type="text" id="emoji-key" name="emoji-key" />
    </div>

    <input type="submit" value="Decrypt Message" />
</form>
