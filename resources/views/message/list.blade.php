<?php /** @var \App\Message[] $messages */ ?>
@extends('layout')

@section('title', "All Messages")

@section('content')
    @if($errors->any())
        <div style="color:red;">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @include('message.new')

    @forelse ($messages as $message)
        <div style="border: 1px solid black; margin-top: 1rem; margin-bottom: 1rem;">
            <div><strong>From: </strong> {{ $message->from }}</div>

            @include('message.decrypt', [ 'message_id' => $message->id ])
        </div>
    @empty
        <em>No messages, create a new message!</em>
    @endforelse
@stop
