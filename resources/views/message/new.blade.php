<form action="{{ URL::route('messages.encrypt') }}" method="post" style="margin: 1rem; border: 1px solid black; padding: 2rem;">
    @csrf
    <div>
        <label for="from">From:</label>
        <input type="text" name="from" id="from" placeholder="Your name" />
    </div>

    <div>
        <textarea name="message" rows="4"></textarea>
    </div>

    <div>
        <label for="emoji-key">Emoji Key:</label>
        <input type="text" name="emoji-key" id="emoji-key" />
    </div>

    <div>
        <input type="submit" value="Encrypt Message" />
    </div>
</form>
